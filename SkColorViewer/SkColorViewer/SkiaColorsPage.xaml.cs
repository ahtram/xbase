﻿using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Threading.Tasks;

using SkiaSharp;
using SkiaSharp.Views.Forms;
using Acr.UserDialogs;

namespace SkColorViewer {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SkiaColorsPage : ContentPage {

        private readonly Dictionary<string, SKColor> _skiaColorsDict = new Dictionary<string, SKColor> {
            { nameof(SKColors.AliceBlue), SKColors.AliceBlue },
            { nameof(SKColors.PaleGreen), SKColors.PaleGreen },
            { nameof(SKColors.PaleGoldenrod), SKColors.PaleGoldenrod },
            { nameof(SKColors.Orchid), SKColors.Orchid },
            { nameof(SKColors.OrangeRed), SKColors.OrangeRed },
            { nameof(SKColors.Orange), SKColors.Orange },
            { nameof(SKColors.OliveDrab), SKColors.OliveDrab },
            { nameof(SKColors.Olive), SKColors.Olive },
            { nameof(SKColors.OldLace), SKColors.OldLace },
            { nameof(SKColors.Navy), SKColors.Navy },
            { nameof(SKColors.NavajoWhite), SKColors.NavajoWhite },
            { nameof(SKColors.Moccasin), SKColors.Moccasin },
            { nameof(SKColors.MistyRose), SKColors.MistyRose },
            { nameof(SKColors.MintCream), SKColors.MintCream },
            { nameof(SKColors.MidnightBlue), SKColors.MidnightBlue },
            { nameof(SKColors.MediumVioletRed), SKColors.MediumVioletRed },
            { nameof(SKColors.MediumTurquoise), SKColors.MediumTurquoise },
            { nameof(SKColors.MediumSpringGreen), SKColors.MediumSpringGreen },
            { nameof(SKColors.LightSlateGray), SKColors.LightSlateGray },
            { nameof(SKColors.LightSteelBlue), SKColors.LightSteelBlue },
            { nameof(SKColors.LightYellow), SKColors.LightYellow },
            { nameof(SKColors.Lime), SKColors.Lime },
            { nameof(SKColors.LimeGreen), SKColors.LimeGreen },
            { nameof(SKColors.Linen), SKColors.Linen },
            { nameof(SKColors.PaleTurquoise), SKColors.PaleTurquoise },
            { nameof(SKColors.Magenta), SKColors.Magenta },
            { nameof(SKColors.MediumAquamarine), SKColors.MediumAquamarine },
            { nameof(SKColors.MediumBlue), SKColors.MediumBlue },
            { nameof(SKColors.MediumOrchid), SKColors.MediumOrchid },
            { nameof(SKColors.MediumPurple), SKColors.MediumPurple },
            { nameof(SKColors.MediumSeaGreen), SKColors.MediumSeaGreen },
            { nameof(SKColors.MediumSlateBlue), SKColors.MediumSlateBlue },
            { nameof(SKColors.Maroon), SKColors.Maroon },
            { nameof(SKColors.PaleVioletRed), SKColors.PaleVioletRed },
            { nameof(SKColors.PapayaWhip), SKColors.PapayaWhip },
            { nameof(SKColors.PeachPuff), SKColors.PeachPuff },
            { nameof(SKColors.Snow), SKColors.Snow },
            { nameof(SKColors.SpringGreen), SKColors.SpringGreen },
            { nameof(SKColors.SteelBlue), SKColors.SteelBlue },
            { nameof(SKColors.Tan), SKColors.Tan },
            { nameof(SKColors.Teal), SKColors.Teal },
            { nameof(SKColors.Thistle), SKColors.Thistle },
            { nameof(SKColors.SlateGray), SKColors.SlateGray },
            { nameof(SKColors.Tomato), SKColors.Tomato },
            { nameof(SKColors.Violet), SKColors.Violet },
            { nameof(SKColors.Wheat), SKColors.Wheat },
            { nameof(SKColors.White), SKColors.White },
            { nameof(SKColors.WhiteSmoke), SKColors.WhiteSmoke },
            { nameof(SKColors.Yellow), SKColors.Yellow },
            { nameof(SKColors.YellowGreen), SKColors.YellowGreen },
            { nameof(SKColors.Turquoise), SKColors.Turquoise },
            { nameof(SKColors.LightSkyBlue), SKColors.LightSkyBlue },
            { nameof(SKColors.SlateBlue), SKColors.SlateBlue },
            { nameof(SKColors.Silver), SKColors.Silver },
            { nameof(SKColors.Peru), SKColors.Peru },
            { nameof(SKColors.Pink), SKColors.Pink },
            { nameof(SKColors.Plum), SKColors.Plum },
            { nameof(SKColors.PowderBlue), SKColors.PowderBlue },
            { nameof(SKColors.Purple), SKColors.Purple },
            { nameof(SKColors.Red), SKColors.Red },
            { nameof(SKColors.SkyBlue), SKColors.SkyBlue },
            { nameof(SKColors.RosyBrown), SKColors.RosyBrown },
            { nameof(SKColors.SaddleBrown), SKColors.SaddleBrown },
            { nameof(SKColors.Salmon), SKColors.Salmon },
            { nameof(SKColors.SandyBrown), SKColors.SandyBrown },
            { nameof(SKColors.SeaGreen), SKColors.SeaGreen },
            { nameof(SKColors.SeaShell), SKColors.SeaShell },
            { nameof(SKColors.Sienna), SKColors.Sienna },
            { nameof(SKColors.RoyalBlue), SKColors.RoyalBlue },
            { nameof(SKColors.LightSeaGreen), SKColors.LightSeaGreen },
            { nameof(SKColors.LightSalmon), SKColors.LightSalmon },
            { nameof(SKColors.LightPink), SKColors.LightPink },
            { nameof(SKColors.Crimson), SKColors.Crimson },
            { nameof(SKColors.Cyan), SKColors.Cyan },
            { nameof(SKColors.DarkBlue), SKColors.DarkBlue },
            { nameof(SKColors.DarkCyan), SKColors.DarkCyan },
            { nameof(SKColors.DarkGoldenrod), SKColors.DarkGoldenrod },
            { nameof(SKColors.DarkGray), SKColors.DarkGray },
            { nameof(SKColors.Cornsilk), SKColors.Cornsilk },
            { nameof(SKColors.DarkGreen), SKColors.DarkGreen },
            { nameof(SKColors.DarkMagenta), SKColors.DarkMagenta },
            { nameof(SKColors.DarkOliveGreen), SKColors.DarkOliveGreen },
            { nameof(SKColors.DarkOrange), SKColors.DarkOrange },
            { nameof(SKColors.DarkOrchid), SKColors.DarkOrchid },
            { nameof(SKColors.DarkRed), SKColors.DarkRed },
            { nameof(SKColors.DarkSalmon), SKColors.DarkSalmon },
            { nameof(SKColors.DarkKhaki), SKColors.DarkKhaki },
            { nameof(SKColors.DarkSeaGreen), SKColors.DarkSeaGreen },
            { nameof(SKColors.CornflowerBlue), SKColors.CornflowerBlue },
            { nameof(SKColors.Chocolate), SKColors.Chocolate },
            { nameof(SKColors.AntiqueWhite), SKColors.AntiqueWhite },
            { nameof(SKColors.Aqua), SKColors.Aqua },
            { nameof(SKColors.Aquamarine), SKColors.Aquamarine },
            { nameof(SKColors.Azure), SKColors.Azure },
            { nameof(SKColors.Beige), SKColors.Beige },
            { nameof(SKColors.Bisque), SKColors.Bisque },
            { nameof(SKColors.Coral), SKColors.Coral },
            { nameof(SKColors.Black), SKColors.Black },
            { nameof(SKColors.Blue), SKColors.Blue },
            { nameof(SKColors.BlueViolet), SKColors.BlueViolet },
            { nameof(SKColors.Brown), SKColors.Brown },
            { nameof(SKColors.BurlyWood), SKColors.BurlyWood },
            { nameof(SKColors.CadetBlue), SKColors.CadetBlue },
            { nameof(SKColors.Chartreuse), SKColors.Chartreuse },
            { nameof(SKColors.BlanchedAlmond), SKColors.BlanchedAlmond },
            { nameof(SKColors.Transparent), SKColors.Transparent },
            { nameof(SKColors.DarkSlateBlue), SKColors.DarkSlateBlue },
            { nameof(SKColors.DarkTurquoise), SKColors.DarkTurquoise },
            { nameof(SKColors.IndianRed), SKColors.IndianRed },
            { nameof(SKColors.Indigo), SKColors.Indigo },
            { nameof(SKColors.Ivory), SKColors.Ivory },
            { nameof(SKColors.Khaki), SKColors.Khaki },
            { nameof(SKColors.Lavender), SKColors.Lavender },
            { nameof(SKColors.LavenderBlush), SKColors.LavenderBlush },
            { nameof(SKColors.HotPink), SKColors.HotPink },
            { nameof(SKColors.LawnGreen), SKColors.LawnGreen },
            { nameof(SKColors.LightBlue), SKColors.LightBlue },
            { nameof(SKColors.LightCoral), SKColors.LightCoral },
            { nameof(SKColors.LightCyan), SKColors.LightCyan },
            { nameof(SKColors.LightGoldenrodYellow), SKColors.LightGoldenrodYellow },
            { nameof(SKColors.LightGray), SKColors.LightGray },
            { nameof(SKColors.LightGreen), SKColors.LightGreen },
            { nameof(SKColors.LemonChiffon), SKColors.LemonChiffon },
            { nameof(SKColors.DarkSlateGray), SKColors.DarkSlateGray },
            { nameof(SKColors.Honeydew), SKColors.Honeydew },
            { nameof(SKColors.Green), SKColors.Green },
            { nameof(SKColors.DarkViolet), SKColors.DarkViolet },
            { nameof(SKColors.DeepPink), SKColors.DeepPink },
            { nameof(SKColors.DeepSkyBlue), SKColors.DeepSkyBlue },
            { nameof(SKColors.DimGray), SKColors.DimGray },
            { nameof(SKColors.DodgerBlue), SKColors.DodgerBlue },
            { nameof(SKColors.Firebrick), SKColors.Firebrick },
            { nameof(SKColors.GreenYellow), SKColors.GreenYellow },
            { nameof(SKColors.FloralWhite), SKColors.FloralWhite },
            { nameof(SKColors.Fuchsia), SKColors.Fuchsia },
            { nameof(SKColors.Gainsboro), SKColors.Gainsboro },
            { nameof(SKColors.GhostWhite), SKColors.GhostWhite },
            { nameof(SKColors.Gold), SKColors.Gold },
            { nameof(SKColors.Goldenrod), SKColors.Goldenrod },
            { nameof(SKColors.Gray), SKColors.Gray },
            { nameof(SKColors.ForestGreen), SKColors.ForestGreen },
        };

        public SkiaColorsPage() {
            InitializeComponent();
        }

        protected override void OnAppearing() {
            base.OnAppearing();
            Task.WhenAll(RefreshDisplay());
        }

        private async Task RefreshDisplay() {
            UserDialogs.Instance.ShowLoading();
            mainStackLayout.Children.Clear();
            foreach (KeyValuePair<string, SKColor> kvp in _skiaColorsDict) {
                await Task.Delay(1);
                mainStackLayout.Children.Add(new ColorPreviewItem(kvp.Key, kvp.Value.ToFormsColor()));
            }
            UserDialogs.Instance.HideLoading();
        }
    }
}