﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Acr.UserDialogs;

namespace SkColorViewer {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FormsColorsPage : ContentPage {

        private readonly Dictionary<string, Color> _formColorsDict = new Dictionary<string, Color> {
            { nameof(Color.AliceBlue), Color.AliceBlue },
            { nameof(Color.MintCream), Color.MintCream },
            { nameof(Color.MistyRose), Color.MistyRose },
            { nameof(Color.Moccasin), Color.Moccasin },
            { nameof(Color.NavajoWhite), Color.NavajoWhite },
            { nameof(Color.Navy), Color.Navy },
            { nameof(Color.OldLace), Color.OldLace },
            { nameof(Color.MidnightBlue), Color.MidnightBlue },
            { nameof(Color.Olive), Color.Olive },
            { nameof(Color.Orange), Color.Orange },
            { nameof(Color.OrangeRed), Color.OrangeRed },
            { nameof(Color.Orchid), Color.Orchid },
            { nameof(Color.PaleGoldenrod), Color.PaleGoldenrod },
            { nameof(Color.PaleGreen), Color.PaleGreen },
            { nameof(Color.PaleTurquoise), Color.PaleTurquoise },
            { nameof(Color.OliveDrab), Color.OliveDrab },
            { nameof(Color.PaleVioletRed), Color.PaleVioletRed },
            { nameof(Color.MediumVioletRed), Color.MediumVioletRed },
            { nameof(Color.MediumSpringGreen), Color.MediumSpringGreen },
            { nameof(Color.LightSkyBlue), Color.LightSkyBlue },
            { nameof(Color.LightSlateGray), Color.LightSlateGray },
            { nameof(Color.LightSteelBlue), Color.LightSteelBlue },
            { nameof(Color.LightYellow), Color.LightYellow },
            { nameof(Color.Lime), Color.Lime },
            { nameof(Color.LimeGreen), Color.LimeGreen },
            { nameof(Color.MediumTurquoise), Color.MediumTurquoise },
            { nameof(Color.Linen), Color.Linen },
            { nameof(Color.Maroon), Color.Maroon },
            { nameof(Color.MediumAquamarine), Color.MediumAquamarine },
            { nameof(Color.MediumBlue), Color.MediumBlue },
            { nameof(Color.MediumOrchid), Color.MediumOrchid },
            { nameof(Color.MediumPurple), Color.MediumPurple },
            { nameof(Color.MediumSeaGreen), Color.MediumSeaGreen },
            { nameof(Color.Magenta), Color.Magenta },
            { nameof(Color.LightSeaGreen), Color.LightSeaGreen },
            { nameof(Color.PapayaWhip), Color.PapayaWhip },
            { nameof(Color.Peru), Color.Peru },
            { nameof(Color.SpringGreen), Color.SpringGreen },
            { nameof(Color.SteelBlue), Color.SteelBlue },
            { nameof(Color.Tan), Color.Tan },
            { nameof(Color.Teal), Color.Teal },
            { nameof(Color.Thistle), Color.Thistle },
            { nameof(Color.Tomato), Color.Tomato },
            { nameof(Color.Snow), Color.Snow },
            { nameof(Color.Transparent), Color.Transparent },
            { nameof(Color.Violet), Color.Violet },
            { nameof(Color.Wheat), Color.Wheat },
            { nameof(Color.White), Color.White },
            { nameof(Color.WhiteSmoke), Color.WhiteSmoke },
            { nameof(Color.Yellow), Color.Yellow },
            { nameof(Color.YellowGreen), Color.YellowGreen },
            { nameof(Color.Turquoise), Color.Turquoise },
            { nameof(Color.PeachPuff), Color.PeachPuff },
            { nameof(Color.SlateGray), Color.SlateGray },
            { nameof(Color.SkyBlue), Color.SkyBlue },
            { nameof(Color.Pink), Color.Pink },
            { nameof(Color.Plum), Color.Plum },
            { nameof(Color.PowderBlue), Color.PowderBlue },
            { nameof(Color.Purple), Color.Purple },
            { nameof(Color.Red), Color.Red },
            { nameof(Color.RosyBrown), Color.RosyBrown },
            { nameof(Color.SlateBlue), Color.SlateBlue },
            { nameof(Color.RoyalBlue), Color.RoyalBlue },
            { nameof(Color.Salmon), Color.Salmon },
            { nameof(Color.SandyBrown), Color.SandyBrown },
            { nameof(Color.SeaGreen), Color.SeaGreen },
            { nameof(Color.SeaShell), Color.SeaShell },
            { nameof(Color.Sienna), Color.Sienna },
            { nameof(Color.Silver), Color.Silver },
            { nameof(Color.SaddleBrown), Color.SaddleBrown },
            { nameof(Color.LightSalmon), Color.LightSalmon },
            { nameof(Color.MediumSlateBlue), Color.MediumSlateBlue },
            { nameof(Color.LightGreen), Color.LightGreen },
            { nameof(Color.LightPink), Color.LightPink },
            { nameof(Color.Cyan), Color.Cyan },
            { nameof(Color.DarkBlue), Color.DarkBlue },
            { nameof(Color.DarkCyan), Color.DarkCyan },
            { nameof(Color.DarkGoldenrod), Color.DarkGoldenrod },
            { nameof(Color.DarkGray), Color.DarkGray },
            { nameof(Color.Cornsilk), Color.Cornsilk },
            { nameof(Color.DarkGreen), Color.DarkGreen },
            { nameof(Color.DarkMagenta), Color.DarkMagenta },
            { nameof(Color.DarkOliveGreen), Color.DarkOliveGreen },
            { nameof(Color.DarkOrange), Color.DarkOrange },
            { nameof(Color.DarkOrchid), Color.DarkOrchid },
            { nameof(Color.DarkRed), Color.DarkRed },
            { nameof(Color.DarkSalmon), Color.DarkSalmon },
            { nameof(Color.DarkKhaki), Color.DarkKhaki },
            { nameof(Color.DarkSeaGreen), Color.DarkSeaGreen },
            { nameof(Color.CornflowerBlue), Color.CornflowerBlue },
            { nameof(Color.Chocolate), Color.Chocolate },
            { nameof(Color.AntiqueWhite), Color.AntiqueWhite },
            { nameof(Color.Aqua), Color.Aqua },
            { nameof(Color.Aquamarine), Color.Aquamarine },
            { nameof(Color.Azure), Color.Azure },
            { nameof(Color.Beige), Color.Beige },
            { nameof(Color.Bisque), Color.Bisque },
            { nameof(Color.Coral), Color.Coral },
            { nameof(Color.Black), Color.Black },
            { nameof(Color.Blue), Color.Blue },
            { nameof(Color.BlueViolet), Color.BlueViolet },
            { nameof(Color.Brown), Color.Brown },
            { nameof(Color.BurlyWood), Color.BurlyWood },
            { nameof(Color.CadetBlue), Color.CadetBlue },
            { nameof(Color.Chartreuse), Color.Chartreuse },
            { nameof(Color.BlanchedAlmond), Color.BlanchedAlmond },
            { nameof(Color.DarkSlateBlue), Color.DarkSlateBlue },
            { nameof(Color.Crimson), Color.Crimson },
            { nameof(Color.DarkTurquoise), Color.DarkTurquoise },
            { nameof(Color.HotPink), Color.HotPink },
            { nameof(Color.IndianRed), Color.IndianRed },
            { nameof(Color.Indigo), Color.Indigo },
            { nameof(Color.Ivory), Color.Ivory },
            { nameof(Color.Khaki), Color.Khaki },
            { nameof(Color.Lavender), Color.Lavender },
            { nameof(Color.Honeydew), Color.Honeydew },
            { nameof(Color.LavenderBlush), Color.LavenderBlush },
            { nameof(Color.LemonChiffon), Color.LemonChiffon },
            { nameof(Color.LightBlue), Color.LightBlue },
            { nameof(Color.LightCoral), Color.LightCoral },
            { nameof(Color.LightCyan), Color.LightCyan },
            { nameof(Color.LightGoldenrodYellow), Color.LightGoldenrodYellow },
            { nameof(Color.DarkSlateGray), Color.DarkSlateGray },
            { nameof(Color.LawnGreen), Color.LawnGreen },
            { nameof(Color.GreenYellow), Color.GreenYellow },
            { nameof(Color.LightGray), Color.LightGray },
            { nameof(Color.Gray), Color.Gray },
            { nameof(Color.Green), Color.Green },
            { nameof(Color.DarkViolet), Color.DarkViolet },
            { nameof(Color.DeepPink), Color.DeepPink },
            { nameof(Color.DimGray), Color.DimGray },
            { nameof(Color.DodgerBlue), Color.DodgerBlue },
            { nameof(Color.Firebrick), Color.Firebrick },
            { nameof(Color.FloralWhite), Color.FloralWhite },
            { nameof(Color.DeepSkyBlue), Color.DeepSkyBlue },
            { nameof(Color.Fuchsia ), Color.Fuchsia },
            { nameof(Color.Gainsboro), Color.Gainsboro },
            { nameof(Color.GhostWhite), Color.GhostWhite },
            { nameof(Color.Gold), Color.Gold },
            { nameof(Color.Goldenrod), Color.Goldenrod },
            { nameof(Color.ForestGreen), Color.ForestGreen },
        };

        public FormsColorsPage() {
            InitializeComponent();
        }

        protected override void OnAppearing() {
            base.OnAppearing();
            Task.WhenAll(RefreshDisplay());
        }

        private async Task RefreshDisplay() {
            UserDialogs.Instance.ShowLoading();
            mainStackLayout.Children.Clear();
            foreach (KeyValuePair<string, Color> kvp in _formColorsDict) {
                await Task.Delay(10);
                mainStackLayout.Children.Add(new ColorPreviewItem(kvp.Key, kvp.Value));
            }
            UserDialogs.Instance.HideLoading();
        }
    }
}