﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBase;

namespace SkColorViewer {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ColorPreviewItem : ContentView {
        public ColorPreviewItem(string displayName, Color color) {
            InitializeComponent();

            frame.BackgroundColor = color;
            nameLabel.Text = displayName;
            hexLabel.Text = "[" + color.ToHexString() + "]";
            rgbLabel.Text = "(" + color.R.ToString("0.00") + ", " + color.G.ToString("0.00") + ", " + color.B.ToString("0.00") + ")";

            //Decide text color.
            bool whiteLabelColor = ((color.R + color.G + color.B) < 1.5) ? (true) : (false);

            if (whiteLabelColor) {
                nameLabel.TextColor = Color.White;
                hexLabel.TextColor = Color.White;
                rgbLabel.TextColor = Color.White;
            } else {
                nameLabel.TextColor = Color.Black;
                hexLabel.TextColor = Color.Black;
                rgbLabel.TextColor = Color.Black;
            }

        }

    }
}