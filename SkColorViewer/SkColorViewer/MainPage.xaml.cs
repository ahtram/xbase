﻿using System.ComponentModel;
using Xamarin.Forms;

namespace SkColorViewer {
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage {
        public MainPage() {
            InitializeComponent();
        }

        private void SkiaColorsButton_Clicked(object sender, System.EventArgs e) {
            Navigation.PushAsync(new SkiaColorsPage());
        }

        private void FormsColors_Clicked(object sender, System.EventArgs e) {
            Navigation.PushAsync(new FormsColorsPage());
        }
    }
}
